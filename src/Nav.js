import React, { Component } from 'react';
import {Link} from 'react-router-dom';

class Nav extends Component {
    render() {
        return (
            <div className='nav'>
                <Link to="./Home" className='nav-home'> Home </Link>
                <Link to="./About" className='nav-about'> About </Link>
                <Link to="./Form" className='nav-form'> Form</Link>
            </div>
        )
    }
}

export default Nav;