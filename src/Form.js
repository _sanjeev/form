import React, { Component } from "react";
import { Link } from "react-router-dom";
import validator from "validator";

class Form extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      email: "",
      age: "",
      password: "",
      confirmpassword: "",
      checkbox: false,
      nameError: "",
      emailError: "",
      ageError: "",
      passwordError: "",
      confirmpasswordError: "",
      checkedError: false,
      status: false,
    };
    // this.handleClick = this.handleClick.bind(this);
  }

  handleChange = (event) => {
    let value = event.target.value;

    if (event.target.name === "checkbox") {
      value = event.target.checked;
    }

    this.setState({
      [event.target.name]: value,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();

    let errorMsg = {
      nameError: "",
      emailError: "",
      ageError: "",
      passwordError: "",
      confirmpasswordError: "",
      checkedError: "",
      status: true,
    };

    if (validator.isEmpty(this.state.name)) {
      errorMsg.status = false;
      errorMsg.nameError = "Please Enter your name";
    }

    if (!validator.isEmpty(this.state.name)) {
      const name = this.state.name.trimStart(" ");
      const noOfSpaces = this.state.name.split(" ").join("");
      if (this.state.name.length !== name.length) {
        errorMsg.status = false;
        errorMsg.nameError = "Please enter a valid name";
      } else if (!validator.isAlpha(noOfSpaces, "en-GB")) {
        errorMsg.status = false;
        errorMsg.nameError = "Name can contains [a-z, A-Z, space]";
      }
    }

    if (validator.isEmpty(this.state.email)) {
      errorMsg.status = false;
      errorMsg.emailError = "Please Enter an email";
    }

    if (
      !validator.isEmpty(this.state.email) &&
      !validator.isEmail(this.state.email, {
        blacklisted_chars: "~ ` ! @ # $ % ^ & * ( ) - + =",
      })
    ) {
      errorMsg.status = false;
      errorMsg.emailError = "Please Enter a valid email";
    }

    if (validator.isEmpty(this.state.age)) {
      errorMsg.status = false;
      errorMsg.ageError = "Please Enter an age";
    }

    if (
      !validator.isEmpty(this.state.age) &&
      (this.state.age <= 0 || this.state.age > 120)
    ) {
      errorMsg.status = false;
      errorMsg.ageError = "Enter a valid age from 1 to 120";
    }

    if (validator.isEmpty(this.state.password)) {
      errorMsg.status = false;
      errorMsg.passwordError = "Please Enter your password";
    }

    if (
      !validator.isEmpty(this.state.password) &&
      this.state.password.length < 6
    ) {
      errorMsg.status = false;
      errorMsg.passwordError = "Password length at least 6 character";
    }

    if (validator.isEmpty(this.state.confirmpassword)) {
      errorMsg.status = false;
      errorMsg.confirmpasswordError = "Please Enter your confirm password";
    }

    if (
      !validator.isEmpty(this.state.confirmpassword) &&
      this.state.password !== this.state.confirmpassword
    ) {
      errorMsg.status = false;
      errorMsg.confirmpasswordError = "Password didnot match";
    }

    if (this.state.checkbox === false) {
      errorMsg.status = false;
      errorMsg.checkedError = "Agree the terms and conditions";
    }

    this.setState(errorMsg);
  };

  handleClick = () => {
    <Link to="./error" />;
  };

  render() {
    const { name, email, age, password, confirmpassword, checked } = this.state;

    return (
      <div>
        {this.state.status === false ? (
          <form className="container form-group" onSubmit={this.handleSubmit}>
            <h1>SignUp Form</h1>
            <div className="col-12 mx-auto">
              <label className="bold">Name</label>
              <input
                type="text"
                defaultValue={name}
                onChange={this.handleChange}
                placeholder="Enter your name"
                name="name"
                className="form-control input-lg bg"
              />
              <div className="error">{this.state.nameError}</div>
            </div>
            <div className="col-12 mx-auto">
              <label className="bold">Email</label>
              <input
                type="text"
                defaultValue={email}
                onChange={this.handleChange}
                placeholder="Enter your email"
                name="email"
                class="form-control input-lg bg"
              />
              <div className="error">{this.state.emailError}</div>
            </div>

            <div className="col-12 mx-auto">
              <label className="bold">Age</label>
              <input
                type="number"
                defaultValue={age}
                onChange={this.handleChange}
                placeholder="Enter your age"
                name="age"
                className="form-control input-lg bg"
              />
              <div className="error">{this.state.ageError}</div>
            </div>
            <div className="col-12 mx-auto">
              <label className="bold">Password</label>
              <input
                type="password"
                defaultValue={password}
                onChange={this.handleChange}
                placeholder="Enter your password"
                name="password"
                className="form-control input-lg bg"
              />
              <div className="error">{this.state.passwordError}</div>
            </div>
            <div className="col-12 mx-auto">
              <label className="bold">Confirm Password</label>
              <input
                type="password"
                defaultValue={confirmpassword}
                onChange={this.handleChange}
                placeholder="Enter confirm password"
                name="confirmpassword"
                className="form-control input-lg bg"
              />
              <div className="error">{this.state.confirmpasswordError}</div>
            </div>
            <div className="custom-control custom-checkbox">
              <input
                id="check-box"
                type="checkbox"
                defaultValue={checked}
                onChange={this.handleChange}
                name="checkbox"
                className="custom-control-input bg"
              />
              <label htmlFor="check-box" className="bold">
                You agree to the terms of use and acknowledge the privacy
                policy.
              </label>
              <div className="error">{this.state.checkedError}</div>
            </div>

            <div className="submit">
              <button>Submit Form</button>
            </div>

            <p className="para">or</p>
            <p className="para">SignUp With</p>
            <div className="icon-container">
              <Link to="/error">
                <img
                  src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATYAAACjCAMAAAA3vsLfAAABLFBMVEX4+Pj////qQzVChfQ0qFP7vAUxffP7/f+80Pv7+/v7uAAsfPP/vQDqPzD7ugAaokP8y1j3xMH98/LpMB32t7PpOyz0p6Lz+vUspk773dv3v7v97u3pMh/sW1DpNSTpLBf51NH+6b78wgD//PL95rP8z2j/+OdWkPX93Zr81Hrx9v78xj/8wzPI2fuz2rxKr2Tb7t+Pyp0zqkRbtXHJ5c/ylpDtZVzvfnfudW385uXsVkrrSz7ym5b1r6rta2HoIgP2mQDsUDHvbynpNzf0jx74qRLtXi7xfiT1sJ4VdPP925Hg6v3l7f3quACz1KetxvrLtiRrnPaasTiNsfhmrEjduBuztC+BrkChvvlMqk2YzqUqhtA8lrGo1bI4n4ZzvoVBieU9kcY6m5s2o3FRXvtJAAAFKUlEQVR4nO3deXPaRhzG8eBI2FyWQcgWGCSbpol7hMNgSJ3ESeumV9ImbXqSUHq8//dQCTCWhBatVB0z+3u+/1phZj+zK3ZFBu7cQQghhBBCCCGEEEIIIYQQQgghhJBgFQq5/12hkPUoUi0GMYdd1qNJp1jNqMglgEYALhk0u6xHlmTJqQnsltACvUnQhZqwmqBuiauJ6Za8moj3tzTUxHNLYYnaibZM01ETbbqlNNlEm25pqYk13VKbbGJNt/TUhJpuYItSimtUpFUKtkiFH/tx46TabFZPGsfh/23Wo42tUKOuVS/GFVVttQzDaKmq1r5ohrPLerSxFcLs6VhtaZUdRxXNMNuHIeSyHm1s8Q64MVENF9mazjAfnYDNH+3K1PzMlmnmuAG2jWoT03eiOaacOalRYuPZf9TVLTNtPePUJsdLibIDCWYrPFKD0ezMCdjWfbbDMdWWGZeBC5UK24kacFdzL9QgNyJsVZMfzVqmh5hti7kWsxoNtkbcaiTYapr/sUDTDDvPX3nUSLC1fd5DraPU48lhvdmsH04uTcdxi0uNAtsTYxPNvHzqOLgf18c3xwc+NQJsPjc2dbxxZm9cqSHUCLBdem9sWqXqd11V07jVxGertzxqxhVjL1trP+NVE5/N+3agXrAx6rxqwrOdPb92by+ecNNQZpM7xc8dcMaWuQa2dadFWe58sXbTxvGoic52sC9bbl/euBlcj27Js31gTTbb7cVSrcV/zyfNdm/JJnf2v7ImXKUdl5rgbB/uy6s6X1/vqL7bXLBtJK/Z5M7z68exqYnNdr8o39Z58Y3PJbtBEWQ7dbLJxfs+l3yb31rpAUG2Myfbvux3Senu9vIE2T7ad7IdRGErfUyP7cDF9l0ktpf02D5xshXvRWHLP6TH9qmL7RRsGyXGtgc2sHnDvS1SSb2TUmSLY99GcAMSwymh9IoeW7QzqZvN/ywvNJvrCUj5+x98Ltnb6LXbzVdNbDbn87byG13yJ/D00MX2I0W226e75Z8kSRnxsL11suV/p8h281lCWf5ZkiS9x8PGs/8QnG31yVX5F2mR0g1W23OtUcZzI8HZlhve8q/SKv0okO2Ba7YxHu6KzmZtQcrl36Q12zBIzf0+yrq1ic6Ws/Ydf0i3KYPtaq/c21/GZld8trM3kitluk1t13NGYK1R4dlyuuRx2zLfdt03NtZTIwpsXcXrNmS9L7y8640JLDxbruedb7ruv+2dvst7Jttrwmx973SzJ9wmXFdSzt+XOCcbAbbcdNNNV3oz51LtDyT7ovM/XW+jjBMCEbbc0LtMFytVkebTrtVsMFSU1RV676/1Qs0zTvFk2I582JZ0umKJ6Y4/6+d/rxcq43/NkGHzu70xO/9nOd8YT8MpseVGYdz+Xagxt2yE2EK56dK7fIl1GKXFZrkxbnC+E+7924CXo8KW60v8bsos6NXIsOUKc86FqnM8PKfDZh9PeSYc+8wqIBvXdxwdzQPhdJ4H5+J8xxHnN2r1h1vhdGXAMdXosVlwc4UhZx23pnxoBNmspTqzD6FuOuuYpc+5PkYly2bLjQZD+zy6Su/NZ/1QL5D1aGMr1KiXdv3RqNsdjfpHvEtTQDZ8726kwBatNNmyHmuMgS1S+L2EaKXHlvVIYw2/BROttNiyHmfM4XeuopUOW9ajjD+oRQtqkcLvk0YLv4YbsSTVsh5bkkEtWvhd+YglACc+2qJY5YiYrSrEYFegRYYQQgghhBBCCCGEEEIIIYQQQgiR6D+r2JJZIxV41QAAAABJRU5ErkJggg=="
                  alt="Google"
                  className="icon"
                  onClick={this.handleClick}
                />
              </Link>
              <Link to="/error">
                <img
                  src="https://help.twitter.com/content/dam/help-twitter/brand/logo.png"
                  alt="twitter"
                  className="icon"
                />
              </Link>
              <Link to="/error">
                <img
                  src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxEREhUSExMSEhUSEhUXGBcVGBEdFRUXFxIWFxUVGBUZHiggGBolGxUVITEhJSkrLi8uFx8zOjMsNygtLisBCgoKDg0OGxAQGy8mICYtLS0tLS0tLTAtLS0tLS0tLS01LS0tLS0vLS0tLS0tLS0tLS0tLS0tLS0tLS0rLS0tLf/AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAwEBAQEBAAAAAAAAAAAABQYHBAMCAQj/xABKEAABAwECCAcKDQIHAQAAAAABAAIDEQQGBRIhMUFRYXETIlKBkaGxBxQyNEJyksHR0hYXIzNDU1Ric4KTosKz4WSDo7Li8PEV/8QAGwEAAQUBAQAAAAAAAAAAAAAAAAEDBAUGAgf/xAAyEQACAQEEBwgCAwADAAAAAAAAAQIDBBEhMQUSQVGR0fATIjJhcYGhscHhFDPxFUJS/9oADAMBAAIRAxEAPwDcUREAEREAEREAERV3Ct6YYqtj+VeNAPFG92nmrzJJSUcWNVq9OjHWqO5dcfYsSibfh6zw5HSCuptSerIOdUbCWHLRP4TiG8kUA9p61GhRpWj/AMopK+m9lGPu+S5+xcLXfbRFFXa53qHtUTaL0Wt+aQM2Bre3P1qGRMurN5sralvtNTOb9sPq5nZJhad3hTPP5jToBoud1pec7nHeXe1ea/E3eRpVJvOT4ns20PGYkbqrpiwlMM0rxuJ9q4QvRq5baOqdSWxviTVmvBam/SF2whh66VUtZr1P8uNp2g0PQaqqsXuxMO0VYZSZZ0LTWjlJ+7v+y92TDcEnlYp1Go68ykgQcoyrOmLvsVskj8FxGzR0LqGlXHCpG/zXV3yi4o2qT8S4F4RQthw212R4xTr0dGhS7XAioIIOkK0o2mlWV9N3/a9syammr0faIifFCIiACIiACIiACIiACIiAC4cJYRjs7MeR1NQGdx1ALkw/hxlmbynuHFZ6ydHrWdWy2STPL5HFzj0AagNAXEp3YIqrfpKNn7kMZ/C9fPy43bZPDV4pbTVvzcfJBGXe7TuUOF+L7hY5xDWguccwAJJ5lFle8zMVas6s9abvb6uXJH4vuNhccVoLicwoSTzBWrBVziaOndT7gpXnIyDcOlWqw2GKEYsbAwbM53k5TzpY0JPPAs7PoitUxn3VxfDZ78Cj2O6lpkyvDYh94gnoy9amLLcuMfOSOfuAaPWVa0TyoQRcUtFWaGcdZ+fLBfBCxXYsjfoq73P9q6hgezj6FnQFIInNSO5EuNnpQ8MEvRLkcH/yLP8AUs6AueS71kP0QG4vHYVLohwi80hZUKcs4p+yK7LdKE+AXM30cPb1qOnuzMzK0tkGygPQcnWrmiYnY6U9l3oMysVB5Ru9OrjO5InMNHtLTqII/wDV6MV7mia8UcA4aiAQoe23facsRxTyTm5jnCqrRoyosabv8snyEVm1cmQbF3WO1vjPFOTSDpXJLC6M4rgWnb6ta/WlUTc6cr43pr2a63E6lAtVjtjZBkyHSCutU9jyDUGhGpTuDsJB/Fdkdo1O/utBo/Sqqvs6uEtj2Pk/vZuHp0GlrLIk0RFdkcIiIAIiIAIiIAKFvDhttlZodI7wG+s7utdeFcIss0bpX6MgGlxOYBZXb7a+eR0jzVzjzAaANgS3FXpK3/x46kPE/hb/AF3frH8nndI4veS5zjUkr4qvOqm7uYCfanVNWxNPGOknUNu3Qm3Ey9OlOrPUhi303f8ALbPPA2B5bU6jBRo8J5zDZ97/ALmWg4JwRFZm0YKkjK45z7BsC67LZmRMDGNDWtGQD/uU7V0JYxSNZYtH07OtbOW/lzzfpgERF0WAREQAREQAREQAREQAREQB42mztkGK4VHWNx0KuYRwY6LKOMzXpG/2q0r5IrkKh2uxU7Su9g9j6zXSaO4TcWUsPQvUjhfBmJV7PA0jk/2UKXrK17JKlNwmuTW/0LihqzV6LRgjCfCcR54wzHlKXWfcKQag0INQRoKt2BsJCdmXw2+EOwjYVoNG2xzXZ1M1k9/7+0RLZZNTvxy2+RJoiK2K8IiIAIirF+MLcDDwbTR81QNjRTGPWBznUlSbdyGq1aNGm6ksl1d75FUvZhrvmajT8nHUN1O1u59CharyBXpFG57g1oJc4gADSSaAKRqXIxVWpKrNzlm+rl9IlMAYJfapQwZGjK92of30f2Wo2SzMiY1jAGtaKAf9znauLAOCm2aIRihccrnconPzDMFKphs1Wj7ErPTx8Tz5c97+CIi5LAIiIAIiIAIiIAIiIAIiIAIiIAIiIA/CFT8P4O4J2M35tx9E8ndqVxXhabO2RhY4VDhQ+3eo1ps6rwueayfWx7SRZq7ozv2bevIzwvXpYLe6GQPGjONY0heeEbK6GR0btGY6xoK4y5UcabhLc0aiMI1I70/lM1CzzNkaHtNQ4VC9lUbmYSymBxz1cz+TfX0q3LQUanaQUjL2qg6FVwft6bOt4RETpHCyK9GE++LS9wPFacVm4aec16Vol67fwFmkeMjqYrd7sleYVPMsiCmWWnffIodM1/DSXq/pfn4PSqufc9wVUutLhkbkZvplPMDTn2KlRMLnBrRVziGgayTQDpK2jBVibBEyJuZjQK6zncecknnS2nuxu3kfRNn7Sr2jyj9vLhn63HYiIoRpgiIgAiIgAiIgAiIgAiIgAiIgAiIgAiIgAiIgCu3vwdwkXCNHGiynazyujP0qjFy1lzQRQ5QVl2GLHwEz49Adk805W9RVfa6d0tdbczQ6Hr60HSezFen+/Z5WS0Oje17c7HAjm0LUbNOJGNe3M9oI5wsoV5uTa8eF0Zzxuyea7KOvGRZJ3S1d53pihrUlUWcfp/v7LKiIrAzZQu6bbKcFCNOM89jf5Kh1U/f21Y9skboja1o9Gp6yVXqq8s9K6lHjxMlb59paJvzu4YfaZZrhWHhbU1xzQtLufwR1mv5VqqpPcxs1IZZdMjwBua2va49CuyrLXK+q1uwL3RlLUs6e148vhIIiKMWBDXvJFjnIJBxM4z+EFkXfMnLf6Tvatcvj4lP5n8gsfT1PIanmenfMnLf6TvanfMnLf6Tvarld65sFps8czpJmufjVDTHTI9zclWk6FJ/F5ZvrbR0xe4l10JqMzrvmTlv9J3tTvmTlv9J3tWi/F5ZvrbR0xe4nxeWb620dMXuI14hqMzrvmTlv9J3tW04GNbPCTlJhjynOeIFW/i8s31to6YvcVrssAYxrBWjGtaK56NAAr0Lick8juMWsyNvaSLHOQSDwZyjPnCyHvmTlv9J3tWu3v8Sn/DPaFjy6p5HM8z075k5b/Sd7U75k5b/Sd7Vdbt3Ps9os0cz3Shz8aoa5gGR7migLToAUp8X9k5c/pM9xLroTUZm3fMnLf6TvaveDCtoYatmmbue+nRWhWhfF9ZOXP6UfuKNwl3PqNJglJI8mTFy7MdtKdHQjXiLqs4sD37njIE4EzNJAAkG3Jkduyb1oditbJmNkjcHNcKgjs2HYsPkYWktcCC0kEHOCDQg86u3cztzhJJATxXN4QbCCGupvDm+iuZxV16FhLHE0NERNDgVPv5ZK8HKPMP8Aub/JXBQ96rPj2Z+tlHD8py9VU1XjrU2TLBV7O0QfndxwM5U7cy0YloDdEjXN5xxh2U51BL3wdPwcsb+S9p5g4V6lWwlqyTNXaKfaUpQ3p/o1dERXFxh7zErxS49qndrmf0BxA6gFHVXthF1ZZDre4/uK56rVwp91LyRjqvjk/N/bNeuHFi2GH72Oel7qdVFYlF3ZbSyQfhM6xVSizFd31Zer+zW0I6tKMVsS+giImh0hb4+JT+Z/ILH1sF8fEp/M/kFj6ep5DU8zXLj+Iw7n/wBV6nll+Bb6us0DIRA1+JXjF5FauLs2KaZ13fGM/wCzN/UPuLhwd50pI0JFnvxjP+zN/UPuJ8Yz/szf1D7iTUkLrxNCRRd3sKG1QNmLMQuLhi1rTFcRnoNSlFydENe/xKf8M9oWPLYb3+JT/hntCx5PU8hqeZrlxfEYf8z+q9TyzPAd9e9oGQ8Bj4mNxselavLs2Kda7/jG/wAN/q/8Fw4O86UkX1FQvjG/w3+r/wAFH4Sv7aJGlsbGw10glzxuJAA6EakhddEPexzTbJy3NwlMmsAB37gVMdzWEm1PfoZCana5zaDqd0KCwVga0Wp1ImFwrlecjRvcf7lajdvAbLHFiA4z3Gr3co6hqaNA3613JpK44ir3eTKIiZHQue2x40b28pjh0tIXQiLrxU7nejISlV+yihI1E9q+VT3G8vxvLr8JtqKm45X4n+3mVv8AxlEgsICkkg1Pd2leFV2YfjxbVO3VNIP3uXBVejwjgmeS1FdJo3C7RrZIPwWf7QpRV+402NYoTqDm9Ejh2AKwLHV1q1Zp739mroyUqcWty+giImhwhb4+JT+Z/ILH1sF8fEp/M/kFj6ep5DU8zus2BrTI0PZDI9rszmtJBoaHLvBXp8HrZ9nm9ErSrj+Iw7n/ANV6nly6jvF1EzF/g9bPs83olPg9bPs83olbQiO0YdmiAuVZZIrIxkjSxwc80dnFXkjIp9ETbd7vHEQ17/Ep/wAM9oWPLYb3+JT/AIZ7QseT1PIanmSNkwBapWCSOFz2OrQgtoaEg5zrBXt8Frd9nf0s9q0W4viMP+Z/Vep5cuo7xVBGOfBa3fZ39LPavOa7tsYKmzy8zcb/AG1WzojtGLqIwmCd8Tqsc6Nw0tJBGzIr7dC97pXCC0EYzsjJMgxjyXDNU6Dpzb/rukYNZwTbQAA8PDXEeU0g0rrIIHWs9a4ggg0INQRnBGYrtJTRxjFm8ouXBlp4WGOT6yNructBK6kwPBEXhaZMRjnclrj0AlAXX4GUTO4x3ntXwvyqVVc4m9uxPtFYfg+5fqXspEP+bR3lXv5Z+Dt0up5a4fmaK9YcoBXnuq2P5SGYDwmOaTtacZvU53QqIvQrFNVLPCXldwwPKLVDUrSXnfxxNR7ltqxrNJHpjlrzPAI6w5XVZT3MbdwdpdGTkmYQPOZxh1Yy1ZZ3SlLUtMvPHn83l1YJ61CPlhwy+LgiIq8mELfHxKfzP5BY+tgvj4lP5n8gsfT1PIanma5cfxGHc/8AqvU8oG4/iMO5/wDVep5NPNjiyCIiQUIiIAhr3+JT/hntCx5bDe/xKf8ADPaFjyep5DU8zV7k2hgsUILmg8fISK/OvU533Hy2ek1YZRfmKEOnftBTN077j5bPSavObCUDBV0sTRrL2D1rD8UL9ojsvMXtC334vJHaA2CE4zGuxnPygOIBADa5xlOXcqgBqyoxpJAAJJzAAkncBnV7uddF7XttFobi4pqyM566HP1U0DPXPSlF1eoo5xky54Js5igijOdkbGneGgHrXYiKOPBRN5bRwdmlOtmKPzUb6ypZU7ugWyjY4h5Ti87mggdZPQkeRKsVPtLRCPnf7LFlMxl7WGLHkYzlva30nAetc9VN3Os3CWph0MBeebIP3EJnUNdVqdnCU9ybNLRET5hrit39wfw1jfQVdERIPy+F+0uWOr+gpGgggioIoRrBzrDcPYNNmtEkJzNdxdrTlaegjrWi0JWvhKk9mK9Hn+OJT6UpXNVPbl+eBzWC1uhkZK3wo3tcNtDWm45udbvZLS2VjJGGrZGhwOwioWArSu5jhjGY6zOOWOr2bWk8ZvM41/NsTmmbPr0lUWcc/R8n+TjRtbVm4Pb9r9F8REWZLw5rbZGTMdHIMZjxQipFRvGUKH+BVg+qP6k3vKwolTaEuTOXB9jjgjbFGMVja0FSaVJJynLnJXUiJBQiIgAiIgDntlmZKx0bxjNeKEVIqN4yhQ3wKsH1R/Um95WFEqbQNJle+BVg+qP6k3vJ8CrB9Uf1JveVhRGs94lyK98CrB9Uf1JveX625tgH0Nd75j1YysCI1nvC5HHYsGww/NRRx+a1oJ3kZSuxESChERABZTea38NaHuBq1pxG+a3JXnNTzq8Xuwp3vAaGj5asbrFRldzDrIWYhdxhery90PQwdZ+i/P4+UfdVee5/Y6MfKfKIY3c0VPWepUaNhcQ1oqXEADWSaALXMF2IQwsiHkNAO053HnNUklcSNL19SjqLOX0sfu47URFyZoKid03A2PG20tHGi4rtrCch5nHocdSva8Z4Wva5jgHNcC0g5iCKEHmT9mryoVVUWz5W1DVakqtNwe0/n9dODLc+zysmjPGjdUaiMxadhFRzrrvLgd1jndEalvhMdymnNzjMdoUWtpGUakL1imuKfWJmJKUJXPBp/K6wN4wVhBloiZNGateK7QdLTtBqF3LHrkXj70kxHk8BIRX7jsweBq0HZuodejeCAQQQRUEZiDmIKyFusjs1S7/q8n1tX72mjstoVaF+3aut+w+0RFDJIREQAREQAREQAREQAREQAREQAREQAXnLIGguJAABJJzADOV6KgX4vDUmzRHMflHDSRnjGwaejWu6cHN3Ifs9nlXmoR93uRCXkwsbTMXeQ3isH3ddNZz9GpRVV81XvYrM+V7Y2CrnGgHrOzTzKa4JK41tOMacVFYJdf6We4WC8eQzOHFjzbXkeoZecLQ1w4Jwe2zxNibmaMp5Tjlc47yu5QZu93mVttp/kVXLZkvT95hERckUIiIAgL2YBbbIcUZJGVdG7bpadhzdB0LG5onMcWOBa5pIIOcEZwV/Qapl+bq98Azwj5Zo4zR9K0fyGjXm1K40Xb1SfZVH3Xk9z5P7K232R1F2kM1mt/73GWK63HvdwNLPOfkz4Dz9HsP3ezdmpRCLQ16EK0HTmsPp7/UqKNaVKWtH/T+hGkEVGUFfSyS6V8X2WkUtZINHKj83W3Z0ajqNhtsczBJG4Pa7MR2HUdiyNrsVSzSulitj2fp+RobPaYVl3c93WzzOpERRCQEREAEREAEREAEREAEREAEXlNIGglxAAFSSaADWSqFei+eNWKzEjQ6TTuYdA+90a07Rozqu6KHqFCdaWrH3exEhfC9IjBhhNZMznDyNbQeX2b82e4y+Kr9qraFBU43I01moQoR1Y+739fHFv7qtHuVgHgWcM8UkeMgOdjD2OOno1qKuXdrGLbRMMgyxtPlHQ8jk6tefNn0FQLTVT7i9+RW6Stt67Gm/V/jnw3hERQylCIiACIiACIiAKTfO6AnrPAAJc7m5AJdux3b1rMJGFpLXAtLTQggggjOCDmK/oVVu811YbYMb5uYDI8DPqDx5Q25x1K5sGlOyup1vDse1c18rzKy12DtO/Tz2reY6u/A+GJ7K/HheW1ztOVjvObp351+YXwRNZX4kzC06DnY4a2u07s64Vo+7UjscX7p9dY5V9GEoy3NGrYCv3Z5qNm+QfrJ+TO5/k/mpvKtrXAioyg6Qv58UlgnDtpsvzUrgOScrD+U5BvFCqa06FhLGi7vJ5cc18l7RlKSxN0RZ5g3uk5hPD+aI/wAHe1WOx3xsUn0wYdUnF6zk61U1dHWmnnBv0x+iT2c9xYEXNDbYn+BJG/zXNPYV0qG007mcBEXhNaWMyue1vnEDtSAe6KEtl6bFFWs7HEaGHHP7aqAt/dFjGSGJzzreQ0dAqT1KVTsVoqeGD98FxdyHoUKk8osvSr2Gr2WazVbjcJIPJZQ0P3nZm9uxZ1hO9NqtFQ6QtafIZVrdxplPOSodWNHRF2NV+y58uJOo6P21H7LmTeHLyT2o0ccRlcjG1xdleUd/QFDhfIXRYrJJK8MjaXE5g3Pv2DaVYdnGEbkrki3pxjCNywSPiqut0bpF1J520bnaw53anOGhuzTuzyl2rnMgpJNSSTOG+Qw6/vO2/wDqt6p7VbE+7T48uZWWvSN61KL9+XPhvPwBfqIqwpwiIgAiIgAiIgAiIgAiIgDlttjjmYWSMa9pzhwqN+w7VQcPdzxwq+zOqPq3njDzX6dx6StIRSbNa61nd9N+2zr0uOJ04zzMBtdkkidiSMdG4aHAg/3G1eK3q22KKZuJKxsjdTgD0aiqnhTud2d9TC90J1HjM68o6Vf2fTdGWFVar4rn7YjlJKLxMxRWa33FtsfgtbKNcbsvouoeiqgrXg6aL5yKSPz2OA6SKK2pV6dXwST9HjwLajKLWDOYE616NtDxme8bnOXmvxPO/Jkhns61SHO953ud7V8FxOclfKApFhkCPoIF62aySSGkbHvOpjXO7ApywXMtsv0fBjXIcX9uV3Uo9WpCn45JeuArnGHid3XH4K+F6RxlxDWguJzAAkncAtAwb3O2ChnkLvux5B6RFT0BW3BuCILOKRRtZrIHGO9xylVFfStGOEO8+C+eQxPSFOPhx+jP8CXFmko6c8C3VkMh5szefoWgYLwTDZm4sTA3Wc7nbXOOUqQRUlotdSv4nhuWRXVrTUq+J4btgREUYjhERABERABERABERABERABERABERABERABERIxGZjfjwuc+tUpy/EW20d/Qi4sv9Z+tVhun84ERO2z+pjlbwM1+DwR5o7F9oiwu0o0EREChERABERABERABERABERAH/9k="
                  alt="Meta"
                  className="icon"
                  onClick={this.handleClick}
                />
              </Link>
            </div>
          </form>
        ) : (
          <div className="status">
            <h1>Form Submitted SuccessFully</h1>
            <p>Name: {this.state.name}</p>
            <p>Email: {this.state.email}</p>
            <p>Age: {this.state.age}</p>
          </div>
        )}
      </div>
    );
  }
}

export default Form;
