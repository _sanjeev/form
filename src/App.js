import React from 'react';
import Form from './Form';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from './Home';
import About from './About';
import Nav from './Nav';
import Error from './Error';
import FormValidation from './FormValidation';

class App extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Router>
        <div className="main">
          <Nav />
          <Switch>
            {/* <Router exact path="/Form">
              <Form />
            </Router> */}
            <Route exact path='/Home' component={Home} />
            <Route exact path='/Form' component={Form} />
            <Route exact path="/About" component={About} />
            <Route exact path='/error' component={Error} />
            <Route exact path="/" component={Home}/>
          </Switch>

        </div>
      </Router>
    )
  }
}

export default App;